# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Slice full page screenshots into viewport screenshots."""

import logging
import os

import admincer.util as util


def _slice_boxes(width, height, step):
    """Yield coordinates of possible slices."""
    if width >= height:
        yield (0, 0, width, height)
        return
    for y_offset in range(0, height - width + 1, step):
        yield (0, y_offset, width, y_offset + width)


def _intersect(box1, box2):
    """Compute intersection of two boxes."""
    x0 = max(box1[0], box2[0])
    y0 = max(box1[1], box2[1])
    x1 = min(box1[2], box2[2])
    y1 = min(box1[3], box2[3])

    if x0 >= x1 or y0 >= y1:
        return None

    return (x0, y0, x1, y1) + box1[4:]


def _box_area(box):
    """Compute the area of the box."""
    x0, y0, x1, y1 = box[:4]
    return (x1 - x0) * (y1 - y0)


def _relative_to(box, region):
    """Translate region coordinates to be relative to box."""
    x_org, y_org = box[:2]
    x0, y0, x1, y1 = region[:4]
    return (x0 - x_org, y0 - y_org, x1 - x_org, y1 - y_org) + region[4:]


def _crop_regions(regions, box, min_part):
    """Crop regions to box.

    Discards the ones of which less than min_part remains. Translates the
    remaining ones so that their coordinates are relative to the box.
    """
    result = []

    for region in regions:
        intersected = _intersect(region, box)
        if intersected is None:
            continue
        if _box_area(intersected) / _box_area(region) < min_part:
            continue
        result.append(_relative_to(box, intersected))

    return result


def _separate(slices):
    """Separate empty and non-empty slices into lists."""
    empty = []
    nonempty = []
    for slice in slices:
        if slice[3] == []:
            empty.append(slice)
        else:
            nonempty.append(slice)
    return empty, nonempty


def _reduce(empty, nonempty, empty_p):
    """Reduce the correct list until they approximately reach the percent."""
    empty_n, nonempty_n = len(empty), len(nonempty)
    current_empty_p = empty_n / (empty_n + nonempty_n) * 100

    if empty_p == 0:
        return nonempty

    if empty_p == 100:
        return empty

    if current_empty_p > empty_p:
        target_empty_n = round(nonempty_n * empty_p / (100 - empty_p))
        return empty[:target_empty_n] + nonempty

    target_nonempty_n = round(empty_n * (100 - empty_p) / empty_p)
    return empty + nonempty[:target_nonempty_n]


def slice_all(src_index, tgt_index, step=10, min_part=0.5,
              empty_percent=None):
    """Slice images with regions into square sub-images.

    Note: at the moment only vertical slicing of tall images is supported.

    Parameters
    ----------
    src_index : RegIndex
        Source of images and region maps are taken.
    tgt_index : RegIndex
        Target index (and directory) for the output images and region maps.
    step : int
        The distance to shift the viewport between slices (in pixels).
    min_part : float
        If a region overlaps the viewport by this percentage, it will be
        included in the slice (appropriately cropped and translated).
    empty_percent : int
        The desired percentage of slices without regions

    Raises
    ------
    KeyError
        If tgt_index already contains any of the slices.

    """
    slices = []
    for image_name, regions in sorted(src_index.items()):
        image = src_index.load_image(image_name)
        size = image.size
        width, height = image.size

        for box in _slice_boxes(width, height, step):
            slice_regions = _crop_regions(regions, box, min_part)
            if empty_percent == 0 and slice_regions == []:
                continue
            base_name = os.path.splitext(image_name)[0]
            slice_name = '{}-{}.png'.format(base_name, box[1])
            slices.append((image_name, slice_name, box, slice_regions, size))

    if empty_percent is not None:
        empty, nonempty = _separate(slices)
        slices = _reduce(empty, nonempty, empty_percent)

    previous_image = ''
    for image_name, slice_name, box, slice_regions, size in slices:
        slice_image = src_index.load_image(image_name).crop(box)

        if previous_image != image_name:
            logging.info('## %s (%dx%d)', image_name, *size)
            previous_image = image_name
        logging.info('### slice %s', util.format_box(box))
        if slice_regions:
            for region in slice_regions:
                logging.info('- region %s type %s',
                             util.format_box(region[:4]), region[4])
        else:
            logging.info('- no regions')
        logging.info('- saved as %s', slice_name)

        tgt_index.add_image(slice_name, slice_image, slice_regions)
